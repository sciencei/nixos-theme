{
  inputs = {
  };
  outputs = { self }:
  let
    font = {
      name = "Hasklig";
      size = 11;
      pkg = pkgs: pkgs.hasklig;
    };
    colorImport = import ./colors/weiss-classic.nix;
  in
  {
    inherit font;
    colors = rec {
      inherit (colorImport)
        foreground
        background
        accent
        accent2 
        accent3
        color0
        color1
        color2
        color3
        color4
        color5
        color6
        color7
        color8
        color9
        color10
        color11
        color12
        color13
        color14
        color15;
      black = color0;
      red = color1;
      green = color2;
      yellow = color3;
      blue = color4;
      magenta = color5;
      cyan = color6;
      white = color7;
      bright =  {
        black = color8;
        red = color9;
        green = color10;
        yellow = color11;
        blue = color12;
        magenta = color13;
        cyan = color14;
        white = color15;
      };
    };
  };
}
