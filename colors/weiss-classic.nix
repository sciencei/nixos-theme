{
  foreground = "#fefeff";
  background = "#454547";
  # a bright accent
  accent     = "#b9d3e0";
  # a muted accent
  accent2    = "#5e5e64";
  # a middling accent
  accent3    = "#8c99a2";
  color0     = "#020203";
  color1     = "#ff6969";
  color2     = "#66ff71";
  color3     = "#eeff69";
  color4     = "#6981ff";
  color5     = "#fc66ff";
  color6     = "#66ccff";
  color7     = "#b9d3e0";
  color8     = "#cbcbcd";
  color9     = "#ff9a9a";
  color10    = "#9affa1";
  color11    = "#faff9a";
  color12    = "#9aaaff";
  color13    = "#fd9aff";
  color14    = "#9addff";
  color15    = "#e3edf2";
}
